//
//  SchoolDetailViewModel.swift
//  ManishYadavNYCSchools
//
//  Created by manish yadav on 1/11/23.
//

import Foundation
class SchoolDetailViewModel {
    var selectedSchoolDbn: String
    var dataModel: SchoolDetailModel?
    let schoolDetailNetworkManager: SchoolDetailNetworkManager

    init(dbn: String,
         schoolDetailNetworkManager: SchoolDetailNetworkManager) {
        self.selectedSchoolDbn = dbn
        self.schoolDetailNetworkManager = schoolDetailNetworkManager
    }
    /// To update UI on successful response
    var updateUI: (() -> ())?
    /// To present error alert
    var showError: ((String) -> ())?
    /// Fetch Details of school
    /// - Parameter request: Contains url for school detail
    func fetchSchoolDetail(request: RequestProtocol) {
        self.schoolDetailNetworkManager.fetchSchoolDetail(request: request) { result in
            switch result {
            case .success(let model):
                if model.isEmpty == false {
                    self.dataModel = model.first
                    self.updateUI?()
                } else {
                    self.showError?(NetworkError.emptyValues.description)
                }
                debugPrint("Success:\(model)")
            case .failure(let error):
                debugPrint("Error")
                self.showError?(error.localizedDescription)
            }
        }
    }
}
