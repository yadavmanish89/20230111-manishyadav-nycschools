//
//  SchoolDetailNetworkManager.swift
//  ManishYadavNYCSchools
//
//  Created by manish yadav on 1/11/23.
//

import Foundation

protocol SchoolDetailNetworkProtocol {
    func fetchSchoolDetail(request: RequestProtocol, completion: @escaping (Result<[SchoolDetailModel], NetworkError>) -> Void)
}
class SchoolDetailNetworkManager: SchoolDetailNetworkProtocol {
    private let networkManager: NetworkProtocol
    init(networkManager: NetworkProtocol) {
        self.networkManager = networkManager
    }
    func fetchSchoolDetail(request: RequestProtocol, completion: @escaping (Result<[SchoolDetailModel], NetworkError>) -> Void) {
        networkManager.request(request: request, completion: completion)
    }
}
