//
//  RequestProtocol.swift
//  ManishYadavNYCSchools
//
//  Created by manish yadav on 1/11/23.
//

import Foundation

protocol RequestProtocol {
    var url: String { get }
    var httpHeaders: [String : String]? { get }
    var method: String { get }
}

let baseURL = "https://data.cityofnewyork.us"
enum APIRequest: RequestProtocol {
    case schoolList
    case schoolDetail(String)
    
    var url: String {
        switch self {
        case .schoolList:
            return "\(baseURL)/resource/s3k6-pzi2.json"
        case .schoolDetail(let dbn):
            return "\(baseURL)/resource/f9bf-2cp4.json?dbn=\(dbn)"
        }
    }
    var httpHeaders: [String: String]? {
        return nil
    }
    var method: String {
        return "GET"
    }
}
