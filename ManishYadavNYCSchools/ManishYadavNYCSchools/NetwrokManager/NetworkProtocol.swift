//
//  NetworkProtocol.swift
//  ManishYadavNYCSchools
//
//  Created by manish yadav on 1/11/23.
//

import Foundation

protocol NetworkProtocol {
    func request<T: Decodable>(request: RequestProtocol,
                               completion: @escaping(Result<T, NetworkError>) -> Void)
}
