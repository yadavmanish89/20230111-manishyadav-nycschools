//
//  SchoolListViewModel.swift
//  ManishYadavNYCSchools
//
//  Created by manish yadav on 1/11/23.
//

import Foundation
protocol SchoolListViewProtocol {
    var updateUI: (() -> ())? { get }
}

class SchoolListViewModel: SchoolListViewProtocol {
    private var dataModel = [SchoolModel]()
    let schoolListNetworkManager: SchoolListNetworkManager
    init(schoolListNetworkManager: SchoolListNetworkManager) {
        self.schoolListNetworkManager = schoolListNetworkManager
    }
    /// To update UI on successful response
    var updateUI: (() -> ())?
    /// To present error alert
    var showError: ((String) -> ())?
    /// Fetch list of schools
    /// - Parameter request: Contains url for fetch schoolList
    func fetchSchoolList(request: RequestProtocol) {
        self.schoolListNetworkManager.fetchSchoolList(request: request) { result in
            switch result {
            case .success(let modelArray):
                self.dataModel = modelArray
                self.updateUI?()
                debugPrint("Success:\(modelArray.count)")
            case .failure(_):
                debugPrint("Error")
            }
        }
    }
    /// Return ModelObject for index
    /// - Parameter index: row index of cell
    /// - Returns: Model object to be populated
    func itemAtIndexPath(index: Int) -> SchoolModel {
        return self.dataModel[index]
    }
    /// Return number of records for table
    func numberOfRows() -> Int {
        return self.dataModel.count
    }
    /// Create url for fetching schoolList
    /// - Returns: API request having url for SchoolList
    func getSchoolListRequest() -> RequestProtocol {
        return APIRequest.schoolList
    }
}
