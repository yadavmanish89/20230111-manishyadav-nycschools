//
//  SchoolModel.swift
//  ManishYadavNYCSchools
//
//  Created by manish yadav on 1/11/23.
//

import Foundation

struct SchoolModel: Codable {
    var dbn: String?
    var school_name: String?
    var phone_number: String?
    var primary_address_line_1: String?
    var city: String?
    var zip: String?
}
