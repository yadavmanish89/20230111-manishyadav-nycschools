//
//  SchoolListNetworkManager.swift
//  ManishYadavNYCSchools
//
//  Created by manish yadav on 1/11/23.
//

import Foundation

protocol SchoolListNetworkProtocol {
    func fetchSchoolList(request: RequestProtocol, completion: @escaping (Result<[SchoolModel], NetworkError>) -> Void)
}
class SchoolListNetworkManager: SchoolListNetworkProtocol {
    private let networkManager: NetworkProtocol
    init(networkManager: NetworkProtocol) {
        self.networkManager = networkManager
    }
    func fetchSchoolList(request: RequestProtocol, completion: @escaping (Result<[SchoolModel], NetworkError>) -> Void) {
        networkManager.request(request: request, completion: completion)
    }
}
