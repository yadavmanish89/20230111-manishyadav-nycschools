//
//  SchoolListViewController+TableView.swift
//  ManishYadavNYCSchools
//
//  Created by manish yadav on 1/11/23.
//

import Foundation
import UIKit

extension SchoolListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolListCell", for: indexPath) as? SchoolListCell else {
            return UITableViewCell()
        }
        let model = self.viewModel.itemAtIndexPath(index: indexPath.row)
        cell.dataModel = model
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectedSchoolDbn = self.viewModel.itemAtIndexPath(index: indexPath.row).dbn {
            self.pushDetailViewController(dbn: selectedSchoolDbn)
        }
    }
}
