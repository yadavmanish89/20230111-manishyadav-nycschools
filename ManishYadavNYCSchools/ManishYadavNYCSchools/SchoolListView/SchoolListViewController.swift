//
//  SchoolListViewController.swift
//  ManishYadavNYCSchools
//
//  Created by manish yadav on 1/11/23.
//

import UIKit

class SchoolListViewController: UIViewController {
    @IBOutlet weak var schoolListTableView: UITableView!
    var viewModel: SchoolListViewModel!
    var networkManager: NetworkManager!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpViewModel()
        self.loadData()
        self.title = "NYC Schools"
    }
    
    /// Initialize ViewModel with Network instance
    /// Call back for UI update and to present Error alert
    private func setUpViewModel() {
        self.networkManager = NetworkManager()
        let schoolListNetworkManager = SchoolListNetworkManager(networkManager: networkManager)
        self.viewModel = SchoolListViewModel(schoolListNetworkManager: schoolListNetworkManager)
        self.viewModel.updateUI = { [weak self] in
            DispatchQueue.main.async {
                self?.schoolListTableView.reloadData()
            }
        }
        self.viewModel.showError = { [weak self] errorMessage in
            print("Show error message:\(errorMessage)")
            //Reloading Tableview even if error case so tableview can update id ant steal data
            DispatchQueue.main.async {
                self?.schoolListTableView.reloadData()
            }
        }
    }
    private func loadData() {
        self.viewModel.fetchSchoolList(request: APIRequest.schoolList)
    }
    
    func pushDetailViewController(dbn: String) {
        let stb = UIStoryboard.init(name: "Main", bundle: nil)
        if let schoolDetailViewController = stb.instantiateViewController(withIdentifier: "SchoolDetailViewController") as? SchoolDetailViewController {
            schoolDetailViewController.viewModel = getSchoolDetailViewModel(dbn: dbn)
            self.navigationController?.pushViewController(schoolDetailViewController, animated: true)
        }
    }
    
    private func getSchoolDetailViewModel(dbn: String) -> SchoolDetailViewModel {
        let schoolDetailNetworkManager = SchoolDetailNetworkManager(networkManager: self.networkManager)
        return SchoolDetailViewModel(dbn: dbn, schoolDetailNetworkManager: schoolDetailNetworkManager)
    }
}
