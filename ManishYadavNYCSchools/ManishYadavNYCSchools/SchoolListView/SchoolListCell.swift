//
//  SchoolListCell.swift
//  ManishYadavNYCSchools
//
//  Created by manish yadav on 1/11/23.
//

import UIKit

class SchoolListCell: UITableViewCell {
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var schoolAddress: UILabel!

    var dataModel: SchoolModel? {
        didSet {
            self.setData()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    private func setData() {
        self.schoolName.text = self.dataModel?.school_name
        let primaryAddress = self.dataModel?.primary_address_line_1 ?? ""
        let city = self.dataModel?.city ?? ""
        let zip = self.dataModel?.zip ?? ""
        let address = "\(primaryAddress), \(city) \(zip)"
        self.schoolAddress.text = address
    }
}

