//
//  MockAPIRequest.swift
//  ManishYadavNYCSchoolsTests
//
//  Created by manish yadav on 1/11/23.
//

import Foundation
@testable import ManishYadavNYCSchools

enum MockAPIRequest: RequestProtocol {
    case schoolList
    case schoolDetail(String)
    
    var url: String {
        switch self {
        case .schoolList:
            return "SchoolList"
        case .schoolDetail(_):
            return "SchoolDetail"
        }
    }
    var httpHeaders: [String: String]? {
        return nil
    }
    var method: String {
        return "GET"
    }
}
