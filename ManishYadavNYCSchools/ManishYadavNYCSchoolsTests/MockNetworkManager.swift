//
//  MockNetworkManager.swift
//  ManishYadavNYCSchoolsTests
//
//  Created by manish yadav on 1/11/23.
//

import Foundation
@testable import ManishYadavNYCSchools

class MockNetworkManager: NetworkProtocol {
    func request<T>(request: RequestProtocol, completion: @escaping (Result<T, NetworkError>) -> Void) where T : Decodable {

        if let data = getJsonData(fileName: request.url) {
            let jsonDecoder = JSONDecoder()
            do {
                let responseModel = try jsonDecoder.decode(T.self, from: data)
                completion(.success(responseModel))
            } catch(let err) {
                debugPrint("Errow while parsing:\(err.localizedDescription)")
                completion(.failure(.parsingError))
            }
        } else {
            completion(.failure(.invalidURL))
        }
    }
    private func getJsonData(fileName: String) -> Data? {
        if let filePath = Bundle(for: type(of: self)).path(forResource: fileName, ofType: "json"),
           let data = try? String(contentsOfFile: filePath).data(using: .utf8) {
            return data
        }
        return nil
    }
}
