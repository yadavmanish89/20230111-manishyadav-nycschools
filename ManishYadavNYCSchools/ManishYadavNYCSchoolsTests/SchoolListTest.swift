//
//  SchoolListTest.swift
//  ManishYadavNYCSchoolsTests
//
//  Created by manish yadav on 1/11/23.
//

import XCTest
@testable import ManishYadavNYCSchools

class SchoolListTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testSchoolListJsonData() {
        let mockNetworkManager = MockNetworkManager()
        let schoolListNetworkManager = SchoolListNetworkManager(networkManager: mockNetworkManager)
        let viewModel = SchoolListViewModel(schoolListNetworkManager: schoolListNetworkManager)
        
        viewModel.updateUI = {
            XCTAssertEqual(viewModel.numberOfRows(), 2)
            let model1 = viewModel.itemAtIndexPath(index: 0)
            XCTAssertEqual(model1.dbn, "02M260")
            let model2 = viewModel.itemAtIndexPath(index: 1)
            XCTAssertEqual(model2.dbn, "21K728")
        }
        viewModel.showError = { error in
            XCTFail("")
        }
        viewModel.fetchSchoolList(request: MockAPIRequest.schoolList)
    }
}
